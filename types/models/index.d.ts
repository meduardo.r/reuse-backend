import {
  CreationOptional,
  HasManySetAssociationsMixin,
  InferAttributes,
  InferCreationAttributes,
  Model,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  HasManyGetAssociationsMixin,
} from "sequelize";

export interface ClientModel
  extends Model<
    InferAttributes<ClientModel>,
    InferCreationAttributes<ClientModel>
  > {
  id: CreationOptional<number>;
  admin: boolean;
  name: string;
  cpf: string;
  phoneNumber: string;
  email: string;
  address: string;
  image: string;
  
  // Relação HasMany -> OrderModel
    
  getOrders: HasManyGetAssociationsMixin<any>;
  setOrders: HasManySetAssociationsMixin<any, any>;
}

export interface OrderModel
  extends Model<
    InferAttributes<OrderModel>,
    InferCreationAttributes<OrderModel>
  > {
  id: CreationOptional<number>;
  quantityOrdered: number | string;
  valueOfOrder: number | string;
  
  // Relaçao belongto -> ClientModel
  setClient: BelongsToSetAssociationMixin<any, number>;
  getClient: BelongsToGetAssociationMixin<any>;
  
  // Relação HasMany -> ProductModel

  getProducts: HasManyGetAssociationsMixin<any>;
}

export interface ProductModel
  extends Model<
    InferAttributes<ProductModel>,
    InferCreationAttributes<ProductModel>
  > {
  id: CreationOptional<number>;
  name: string;
  description: string;
  price: number | string;
  amount: number | string;
  formOfPayment: boolean;
  evaluation: string;
  totalPrice: number | string;

  setOrder: BelongsToSetAssociationMixin<any, number>;
}
