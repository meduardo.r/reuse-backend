# reuse-backend

## Resus-e Backend 

É um projeto do TT_2023(treinamento da EJCM) para novos participantes da empresa Júnior, 
tem como objetivo testar e fomentar aos participantes do T.T evoluir e conquistar novas habilidades.

# Projeto
 
O projeto foi proposto para fazer o Backend de um markteplace.

## Objetivo 

Tratar dados de entrada e saída, como compras dos clientes, pedidos, produtos. Forma de aprendizado da construção de um API/RESTful.


## Tecnologias 

Uso do Javascript nodejs que é um tecnologia que permite o uso da Linguagem Javascript no backend e suas bibliotecas

## Bibliotecas usadas no Projeto

* [Express](https://expressjs.com/)
* [Sequelize](https://sequelize.org/)
* [Dotenv](https://github.com/motdotla/dotenv)
* [Sqlite3](https://github.com/TryGhost/node-sqlite3)

## Typescript

Que é tecnologia de código aberto inventada pela Microsoft, que nada mais é uma coleção tipada (annotaion) da linguagem de programação 
Javascript, pode se ver como uma nova linguagem porém não é, para não estender em explicações séra ela a linguagem usada para programar

### Types 
Pela escolha do Typescript deve ser baixado os types junto com as bibliotecas:

* [@types/express](https://github.com/DefinitelyTyped/DefinitelyTyped)
* [@types/node](https://github.com/DefinitelyTyped/DefinitelyTyped)
* [@types/sequilize](https://github.com/DefinitelyTyped/DefinitelyTyped)
* [typescript](https://www.typescriptlang.org/)
* [ts-node-dev](https://github.com/wclr/ts-node-dev) -> (transpilador)


## Comandos do Projeto

~~~bash

git clone https://gitlab.com/meduardo.r/reuse-backend && cd resue-backend

# Instalar todas as dependências 

npm install

# Compilar 

yarn build || npm build

# Criar o banco de dados

yarn run migrate || npm run  migrate 

# Caso queira criar Seeders

yarn run seed || npm run seed

# Iniciar o server para testes

yarn run dev || npm run dev

# Iniciar o server 

yarn run start || npm run start 

~~~

## Observações

* O uso do typescript com sequelize é diferente na construção dos códigos:

~~~typescript
export interface ClientModel
  extends Model<
    InferAttributes<ClientModel>,
    InferCreationAttributes<ClientModel>
  > {
  id: CreationOptional<number>;
  name: string;
  cpf: string;
  phoneNumber: string;
  email: string;
  address: string;
  
  // Relação HasMany -> OrderModel
    
  getOrders: HasManyGetAssociationsMixin<any>;
  setOrders: HasManySetAssociationsMixin<any, any>;
}
~~~

Criação de  interfaces para construção dos models.

* Os `mixins` que são criados de forma automática quando o código é feito em javascript puro, no typescript eles tem que ser 'setados' dentro dos interfaces dos models. 

**Essas interfaces são `tipos` que podem ser chamados por outras funções**

~~~typescript

getOrders: HasManyGetAssociationsMixin<any>;
setOrders: HasManySetAssociationsMixin<any, any>;

~~~

[Namespace e Types](https://www.digitalocean.com/community/tutorials/how-to-use-namespaces-in-typescript#creating-namespaces-in-typescript)

* Namespace & Types:  `namespaces` é terminologia que era usada para `internal modules` 

Declarar os types para o compilador enxergar:

> CompilerOptions{ "typeRoots": ["./types"] }  

[mais sobre namespaces e types](https://www.typescriptlang.org/pt/docs/handbook/namespaces.html)

## Links de consultas

* [Association](https://strapengine.com/sequelize-associations-in-nodejs/#One-To-Many-relation)
* [git](https://githowto.com/pt-BR/creating_a_branch)
* [Cardinalidade](https://consultabd.wordpress.com/2019/08/28/cardinalidade/)
* [Modelagem de banco de dados](https://medium.com/@alberteije/modelagem-de-bancos-de-dados-sem-segredos-parte-03-810828faf93)
* [exemplos de sequelize](https://www.treinaweb.com.br/blog/usando-sequelize-orm-com-node-e-express)

# Tarefas

* [x] Instalar as Bibliotecas;
* [x] Configurar o Typescript;
* [x] push origin main
* [x] checkout -p develop
* [x] Criar o server (express)
* [x] Criar o Banco de Dados;
* [x] Conectar com o server;
* [x] Criar os moldes do banco de dados;
* [x] Criar os relacionamentos dos moldes;
* [x] Criar o Migração do Banco com os moldes;
* [x] Criar o Crud;
* [x] Criar as validações;
* [x] Subir no Git as imagens dos models;
* [x] *Criar os Seeders;
* [x] Testar o Crud no Postman; 
