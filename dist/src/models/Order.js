"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = __importDefault(require("../config/database"));
const Product_1 = __importDefault(require("../models/Product"));
const Order = database_1.default.define("Order", {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    quantityOrdered: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: true,
    },
    valueOfOrder: {
        type: sequelize_1.DataTypes.DOUBLE,
        allowNull: true,
    },
});
Order.hasMany(Product_1.default);
Product_1.default.belongsTo(Order);
exports.default = Order;
