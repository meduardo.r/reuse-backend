"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = __importDefault(require("../config/database"));
function priceTotal(valueOne, valueTwo) {
    let valueTotal = valueOne * valueTwo;
    return valueTotal;
}
const Product = database_1.default.define("Product", {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    price: {
        type: sequelize_1.DataTypes.DOUBLE,
        allowNull: false,
    },
    amount: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    formOfPayment: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    evaluation: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: true,
    },
    totalPrice: {
        type: sequelize_1.DataTypes.INTEGER,
        set() {
            let valueOne = this.getDataValue("price");
            let valueTwo = this.getDataValue("amount");
            let value = priceTotal(valueOne, valueTwo);
            this.setDataValue("totalPrice", value);
        },
        allowNull: true,
    },
});
exports.default = Product;
