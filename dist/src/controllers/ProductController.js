"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Product_1 = __importDefault(require("../models/Product"));
const Order_1 = __importDefault(require("../models/Order"));
// Cadastro do produto
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const product = yield Product_1.default.create(req.body);
        return res
            .status(201)
            .json({ message: "Producto confirmado", Product: product });
    }
    catch (err) {
        res.status(500).json({ error: err });
    }
});
// Leitura de todas fichas dos productes
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const products = yield Product_1.default.findAll();
        return res.status(200).json({ products });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
//Leitura da ficha pro {id} do producte
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const product = yield Product_1.default.findByPk(id);
        return res.status(200).json({ product });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
// Update da entidade Product
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const product = yield Product_1.default.update(req.body, { where: { id: id } });
        return res.status(200).json({ product });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
// Deletar parametros da entidade Product
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const product = yield Product_1.default.destroy({ where: { id: id } });
        return res.status(200).json({ product });
    }
    catch (err) {
        res.status(500).json({ err });
    }
});
const addOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { orderId, productId } = req.params;
    try {
        const order = yield Order_1.default.findByPk(orderId);
        const product = yield Product_1.default.findByPk(productId);
        yield (product === null || product === void 0 ? void 0 : product.setOrder(order));
        res.status(200).json(product);
    }
    catch (error) {
        res.status(500).json({ error });
    }
});
const cancelProduct = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const product = yield Product_1.default.findByPk(id);
        yield (product === null || product === void 0 ? void 0 : product.setOrder(null));
        res.status(200).json(product);
    }
    catch (error) {
        res.status(500).json({ error });
    }
});
exports.default = {
    create,
    index,
    show,
    update,
    destroy,
    addOrder,
    cancelProduct
};
