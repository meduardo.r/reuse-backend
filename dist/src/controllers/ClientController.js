"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __importDefault(require("../models/Client"));
// Criação da ficha do cliente 
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const client = yield Client_1.default.create(req.body);
        return res.status(201).json({ message: "Cliente confirmado", Client: client });
    }
    catch (err) {
        res.status(500).json({ error: err });
    }
});
// Leitura de todas fichas dos clientes
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const clients = yield Client_1.default.findAll();
        return res.status(200).json({ clients });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
//Leitura da ficha pro {id} do cliente
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const client = yield Client_1.default.findByPk(id);
        return res.status(200).json({ client });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
// TODO(Marco): Criar o Update
// Update da entidade Client 
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const [updated] = yield Client_1.default.update(req.body, { where: { id: id } });
        if (updated) {
            const client = yield Client_1.default.findByPk(id);
            return res.status(200).json({ client });
        }
        throw new Error();
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
// TODO(Marco): Criar o Delete
// Deletar parametros da entidade Client
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const client = yield Client_1.default.destroy({ where: { id: id } });
        return res.status(200).json({ client });
    }
    catch (err) {
        res.status(500).json({ err });
    }
});
exports.default = {
    create,
    index,
    show,
    update,
    destroy,
};
// TODO(Marco): Criar as Regras de Relacionamento
