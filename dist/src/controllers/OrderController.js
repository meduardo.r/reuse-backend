"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Order_1 = __importDefault(require("../models/Order"));
const Client_1 = __importDefault(require("../models/Client"));
// Criação do Pedido  
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const order = yield Order_1.default.create(req.body);
        return res.status(201).json({ message: "Order confirmado", Order: order });
    }
    catch (err) {
        res.status(500).json({ error: err });
    }
});
// Leitura de todas fichas dos orderes
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const orders = yield Order_1.default.findAll();
        return res.status(200).json({ orders });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
//Leitura da ficha pro {id} do order
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const order = yield Order_1.default.findByPk(id);
        return res.status(200).json({ order });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
});
// Deletar parametros da entidade Order
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const order = yield Order_1.default.destroy({ where: { id: id } });
        return res.status(200).json({ order });
    }
    catch (err) {
        res.status(500).json({ err });
    }
});
const AddClient = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { clientId, orderId } = req.params;
    try {
        const client = yield Client_1.default.findByPk(clientId);
        const order = yield Order_1.default.findByPk(orderId);
        yield (order === null || order === void 0 ? void 0 : order.setClient(client));
        return res.status(200).json(order);
    }
    catch (error) {
        return res.status(500).json({ error });
    }
});
const cancelOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const order = yield Order_1.default.findByPk(id);
        yield (order === null || order === void 0 ? void 0 : order.setClient(null));
        res.status(200).json(order);
    }
    catch (error) {
        res.status(500).json({ error });
    }
});
exports.default = {
    create,
    index,
    show,
    destroy,
    AddClient,
    cancelOrder
};
