"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = require("@faker-js/faker");
const Client_1 = __importDefault(require("../models/Client"));
const seedClient = function () {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield Client_1.default.sync({ force: true });
            for (let i = 0; i < 20; i++) {
                yield Client_1.default.create({
                    name: faker_1.faker.name.firstName(),
                    cpf: faker_1.faker.phone.imei(),
                    phoneNumber: faker_1.faker.phone.number('+55 21 9#### ####'),
                    email: faker_1.faker.internet.email(),
                    address: faker_1.faker.address.city(),
                });
            }
        }
        catch (err) {
            console.log(err);
        }
    });
};
exports.default = seedClient;
