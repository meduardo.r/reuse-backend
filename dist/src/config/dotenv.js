"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function configDotenv() {
    const result = require('dotenv').config();
    if (result.error) {
        throw result.error;
    }
}
exports.default = configDotenv;
