"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db = new sequelize_1.Sequelize('app', '', '', {
    dialect: 'sqlite',
    storage: process.env.DB_DATABASE,
    logging: false,
});
exports.default = db;
const Client_1 = __importDefault(require("../models/Client"));
const Product_1 = __importDefault(require("../models/Product"));
const Order_1 = __importDefault(require("../models/Order"));
const Store_1 = __importDefault(require("../models/Store"));
let mod = [Client_1.default, Product_1.default, Order_1.default, Store_1.default];
for (let i in mod) {
    for (i in db.models) {
        if (db.models[i].associations instanceof Function) {
            db.models[i].associations;
        }
    }
}
