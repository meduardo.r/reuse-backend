"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ClientController_1 = __importDefault(require("../controllers/ClientController"));
const ProductController_1 = __importDefault(require("../controllers/ProductController"));
const OrderController_1 = __importDefault(require("../controllers/OrderController"));
const router = (0, express_1.default)();
// Rotas do Cliente
router.post("/client", ClientController_1.default.create);
router.get("/client", ClientController_1.default.index);
router.get("/client/:id", ClientController_1.default.show);
router.put("/client/:id", ClientController_1.default.update);
router.delete("/client/:id", ClientController_1.default.destroy);
// Rotas do Pedido
router.post("/order", OrderController_1.default.create);
router.get("/order", OrderController_1.default.index);
router.get("/order/:id", OrderController_1.default.show);
router.delete("/order/:id", OrderController_1.default.destroy);
// Rotas do Produto
router.post("/product", ProductController_1.default.create);
router.get("/product", ProductController_1.default.index);
router.get("/product/:id", ProductController_1.default.show);
router.patch("/product/:id", ProductController_1.default.update);
router.delete("/product/:id", ProductController_1.default.destroy);
// Rotas das relações
router.put("/order/:orderId/product/:productId", ProductController_1.default.addOrder);
router.put("/client/:clientId/order/:orderId", OrderController_1.default.AddClient);
//Remove relações 
router.put("/order/:id", OrderController_1.default.cancelOrder);
router.put("/product/:id", ProductController_1.default.cancelProduct);
exports.default = router;
