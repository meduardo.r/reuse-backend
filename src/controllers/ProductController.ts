import { Request, Response } from "express";
import Product from "../models/Product";
import Order from "../models/Order";

interface newProduct {
  name: string,
  description: string,
  price: number,
  amount: number,
  formOfPayment: boolean,
  evaluation: string,
  totalPrice: number
}

// Cadastro do produto
const create = async (req: Request, res: Response) => {
  try {
    const productNew:(newProduct) = {
      name: '',
      description: '',
      price: 0,
      amount: 0,
      formOfPayment: false,
      evaluation: '',
      totalPrice: 0,
    }
    const product = await Product.create(productNew);
    return res
      .status(201)
      .json({ message: "Producto confirmado", Product: product });
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

// Leitura de todas as requisições da entidade Product 
const index = async (req: Request, res: Response) => {
  try {
    const products = await Product.findAll();
    return res.status(200).json({ products });
  } catch (err) {
    return res.status(500).json({ err });
  }
};

//Leitura da ficha pro {id} do producte
const show = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const product = await Product.findByPk(id);
    return res.status(200).json({ product });
  } catch (err) {
    return res.status(500).json({ err });
  }
};

// Update da entidade Product
const update = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const product = await Product.update(req.body, { where: { id: id } });
    return res.status(200).json({ product });
  } catch (err) {
    return res.status(500).json({ err });
  }
};


// Deletar parametros da entidade Product
const destroy = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const product = await Product.destroy({ where: { id: id } });
    return res.status(200).json({ product });
  } catch (err) {
    res.status(500).json({ err });
  }
};

const addOrder = async (req: Request, res: Response) => {
  
  const { orderId, productId } = req.params;
  try {
    const order = await Order.findByPk(orderId);
    const product = await Product.findByPk(productId);
    await product?.setOrder(order);
    res.status(200).json(product);
  } catch (error) {
    res.status(500).json({ error });
  }
};


const cancelProduct = async(req:Request, res:Response) => {
  const {id} = req.params;
  try {
    const product = await Product.findByPk(id);
    await product?.setOrder(null);
    res.status(200).json(product);
    
  } catch (error) {
    res.status(500).json({error});
  }
};


export default {
  create,
  index,
  show,
  update,
  destroy,
  addOrder,
  cancelProduct
};
