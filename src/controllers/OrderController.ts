import { Request, Response } from 'express';
import Order from '../models/Order';
import Client from '../models/Client';
import Product from '../models/Product';


interface orderNew {
    quantityOrdered: number | string
    valueOfOrder: number | string
}


// Criação do Pedido  
const create = async (req: Request, res: Response) => {
    try {
        const newOrder: (orderNew) = {
            quantityOrdered: ' ',
            valueOfOrder: ' '
        }
        const order = await Order.create(newOrder);
        return res.status(201).json({ message: "Order confirmado", Order: order });
    } catch (err) {
        res.status(500).json({ error: err });
    }
};


// Chamada de todas os dados da entidade Order 
const index = async (req: Request, res: Response) => {
    try {
        const orders = await Order.findAll();
        return res.status(200).json({ orders });
    } catch (err) {
        return res.status(500).json({ err });
    }
};


//Leitura da ficha pro {id} do order
const show = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const order = await Order.findByPk(id, {
            include: [{
                model: Product,
            }]
        });
        return res.status(200).json({ order });
    } catch (err) {
        return res.status(500).json({ err });
    }
};


// Deletar parametros da entidade Order
const destroy = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const order = await Order.destroy({ where: { id: id } });
        return res.status(200).json({ order });
    } catch (err) {
        res.status(500).json({ err });
    }
};



const AddClient = async (req: Request, res: Response) => {
    const { clientId, orderId } = req.params;
    try {
        const client = await Client.findByPk(clientId);
        const order = await Order.findByPk(orderId);
        await order?.setClient(client)
        return res.status(200).json(order);
    } catch (error) {
        return res.status(500).json({ error });
    }
};



const cancelOrder = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const order = await Order.findByPk(id);
        await order?.setClient(null);
        res.status(200).json(order)
    } catch (error) {
        res.status(500).json({ error });
    }
};



export default {
    create,
    index,
    show,
    destroy,
    AddClient,
    cancelOrder
};
