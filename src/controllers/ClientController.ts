
import {Request, Response} from 'express';
import Client from '../models/Client';
import Order from '../models/Order';


// Colocar na pasta type (Organizar a forma que usa os tipos)
interface newClient {
    admin:boolean,
    name:string,
    cpf: string,
    phoneNumber: string,
    email:string,
    address: string
    image: string,

}


// Criação da ficha do cliente 
const create = async(req: Request, res: Response) => {
    try {
        const clientNew:(newClient) = {
            admin: false,
            name: '',
            cpf: '',
            phoneNumber: '',
            email: '',
            address: '',
            image: ''
        }
        const client = await Client.create(clientNew);
	      return res.status(201).json({message: "Cliente confirmado", Client: client});
    }catch(err){
	res.status(500).json({error:err});
    }
};


// Leitura de todas fichas dos clientes
const index = async(req: Request, res: Response) => {
    try {
        const clients = await Client.findAll();
        return res.status(200).json({clients});
    }catch(err){
        return res.status(500).json({err});
    }
};


//Leitura da ficha pro {id} do cliente
const show = async(req: Request, res: Response) => {
    const {id} = req.params;
    try {
        const client = await Client.findByPk(id, {
            include:[{
                model: Order,
            }]
        });
        return res.status(200).json({client});
    }catch(err){
        return res.status(500).json({err});
    }
};


// TODO(Marco): Criar o Update
// Update da entidade Client 
const update = async(req: Request, res: Response) => {
    const {id} = req.params;
    try {
        const [updated] = await Client.update(req.body, {where: {id:id}});
        if (updated){
            const client = await Client.findByPk(id);
            return res.status(200).json({client});
           }     
        throw new Error();
    }catch(err){
        return res.status(500).json({err});
    }
};


// TODO(Marco): Criar o Delete
// Deletar parametros da entidade Client
const destroy = async(req: Request, res: Response) => {
    const {id} = req.params;
    try {
        const client = await Client.destroy({where: {id: id}});
        return res.status(200).json({client});
    }catch(err){
        res.status(500).json({err});
    }
};


export default {
    create,
    index,
    show,
    update,
    destroy,
};

// TODO(Marco): Criar as Regras de Relacionamento
