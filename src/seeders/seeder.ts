import * as dotenv from 'dotenv';
dotenv.config();
import '../config/database';

import seedClient from './seedClient';
import seedProduct from './seedProduct';
import seedStore from './seedStore';
import seedOrder from './seedOrder';

(async () => {
  try {
    await seedClient();
    await seedProduct();
    await seedStore();
    await seedOrder();

  } catch (err) {
    console.log(err);
  }
})();


