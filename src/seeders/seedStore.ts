import { faker } from '@faker-js/faker';
import Store from '../models/Store';


const seedStore= async function () {
    try {
        await Store.sync({force: true});

        for (let i = 0; i < 20; i++) {
           await Store.create({
            soldAmount:faker.random.numeric(2),
           });

        }
    }catch (err){
        console.log(err);
    }
    
};

export default seedStore;