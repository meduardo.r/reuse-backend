import { faker } from '@faker-js/faker';
import Product from '../models/Product';


const seedProduct = async function () {
    try {
        await Product.sync({force: true});

        for (let i = 0; i < 20; i++) {
           await Product.create({
            name: faker.commerce.productName(),
            description:faker.commerce.productDescription(),
            price:faker.commerce.price(),
            amount:faker.random.numeric(2),
            formOfPayment:faker.datatype.boolean(),
            evaluation:faker.lorem.text(),
            totalPrice:faker.commerce.price(),

           });

        }
    }catch (err){
        console.log(err);
    }
    
};

export default seedProduct;