import { faker } from '@faker-js/faker'; 
import Order from '../models/Order';


const seedOrder= async function () {
    try {
        await Order.sync({force: true});

        for (let i = 0; i < 20; i++) {
           await Order.create({
            quantityOrdered:faker.random.numeric(),
            valueOfOrder:faker.commerce.price(),
           });

        }
    }catch (err){
        console.log(err);
    }
    
};

export default seedOrder;