import { faker } from '@faker-js/faker';
import Client from '../models/Client';


const seedClient = async function () {
    try {
        await Client.sync({force: true});

        for (let i = 0; i < 20; i++) {
           await Client.create({
            admin: faker.datatype.boolean(),
            name:faker.name.firstName(),
            cpf:faker.phone.imei(),
            phoneNumber:faker.phone.number('+55 21 9#### ####'),
            email:faker.internet.email(),
            address:faker.address.city(),
            image:faker.image.avatar()
           });

        }
    }catch (err){
        console.log(err);
    }
    
};

export default seedClient;