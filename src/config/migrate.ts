import dotenv from 'dotenv';
dotenv.config();

import db from '../config/database';

(async () => {
  try {
    await db.sync({ force: true });
    console.log('Connection has been established successfully.');
  }
  catch (error) { console.log(error) };
})();
    