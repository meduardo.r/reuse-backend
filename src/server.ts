import * as dotenv from 'dotenv';
dotenv.config();

import express, { Request, Response } from "express";
import db from './config/database';

import routes from './routes/routes';


const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(routes);
app.use(express.urlencoded({ extended: true }));


(async () => {
    await db.sync().then(() => {
        console.log('connect to database!')

        app.listen(port, () => {
            console.log(`App on in ${port}`);
        })

    });
})();

app.get
app.get('/', (req: Request, res: Response) => {
    res.send('Welcome to the Server!!');
})



