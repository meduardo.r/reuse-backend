import express from "express";

import ClientController from "../controllers/ClientController";
import ProductController from "../controllers/ProductController";
import OrderController from "../controllers/OrderController";

const router = express();

// Rotas do Cliente
router.post("/client", ClientController.create);
router.get("/client", ClientController.index);
router.get("/client/:id", ClientController.show);
router.put("/client/:id", ClientController.update);
router.delete("/client/:id", ClientController.destroy);

// Rotas do Pedido
router.post("/order", OrderController.create);
router.get("/order", OrderController.index);
router.get("/order/:id", OrderController.show);
router.delete("/order/:id", OrderController.destroy);

// Rotas do Produto
router.post("/product", ProductController.create);
router.get("/product", ProductController.index);
router.get("/product/:id", ProductController.show);
router.patch("/product/:id", ProductController.update);
router.delete("/product/:id", ProductController.destroy);

// Rotas das relações
router.put("/order/:orderId/product/:productId", ProductController.addOrder);
router.put("/client/:clientId/order/:orderId", OrderController.AddClient);

//Remove relações 
router.put("/order/:id", OrderController.cancelOrder);
router.put("/product/:id", ProductController.cancelProduct);

export default router;
