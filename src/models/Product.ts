import { DataTypes } from "sequelize";
import db from "../config/database";
import { ProductModel } from "../../types/models";


// // Função para calcular os atributos e dar o valor final de (totalPrice)
// function priceTotal(valueOne: any, valueTwo: any) {
//   let valueTotal = valueOne * valueTwo;
//   return valueTotal;
// }

const Product = db.define<ProductModel>("Product", {
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement:true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  price: {
    type: DataTypes.DOUBLE,
    allowNull: false,
  },
  amount: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  formOfPayment: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
    allowNull: false,
  },
  evaluation: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
  totalPrice: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },
});


// Product.belongsToMany(Store);
// Store.belongsToMany(Product);

export default Product;
