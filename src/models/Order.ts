import { DataTypes } from "sequelize";
import db from "../config/database";
import { OrderModel } from "../../types/models";

import Product from "../models/Product";

const Order = db.define<OrderModel>("Order", {
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
  },
  quantityOrdered: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },
  valueOfOrder: {
    type: DataTypes.DOUBLE,
    allowNull: true,
  },
});


// Relações entre as tabelas (HASMANY)

Order.hasMany(Product);
Product.belongsTo(Order);

export default Order;
