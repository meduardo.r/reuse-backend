import {  DataTypes } from 'sequelize';
import db from '../config/database';




const Store = db.define('Store', {
  soldAmount:{
    type:DataTypes.INTEGER,
    allowNull:false,
  },  
});



export default Store;
