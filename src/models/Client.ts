import { DataTypes } from "sequelize";
import db from "../config/database";
import { ClientModel } from "../../types/models";

import Order from "./Order";

const Client = db.define<ClientModel>("Client", {
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
  },
  admin:{
    type: DataTypes.BOOLEAN,
    defaultValue: false,
    allowNull: false
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  cpf: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  phoneNumber: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  address: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  image:{
    type:DataTypes.STRING,
    allowNull: true
  }
});

// Relações entre as tabelas (HASMANY)

Client.hasMany(Order);
Order.belongsTo(Client);

export default Client;
